import math

import cv2
import numpy as np
from PIL import Image
from numpy import ndarray

from oemer import layers
from oemer.notehead_extraction import NoteType, NoteHead

# Globals
out: ndarray


def calculate_angle_between_vectors(vector1, vector2):
    # Calculate the dot product of the vectors
    dot_product = np.dot(vector1, vector2)

    # Calculate the norms (magnitudes) of the vectors
    norm_vector1 = np.linalg.norm(vector1)
    norm_vector2 = np.linalg.norm(vector2)

    # Calculate the cosine of the angle
    cos_theta = dot_product / (norm_vector1 * norm_vector2)

    # Clamp the value of cos_theta to the range [-1, 1] to avoid numerical errors
    cos_theta = np.clip(cos_theta, -1.0, 1.0)

    # Calculate the angle in radians
    angle_radians = np.arccos(cos_theta)

    # Convert the angle to degrees
    angle_degrees = np.degrees(angle_radians)

    return angle_radians, angle_degrees


def draw_dots(
        notes: [NoteHead],
        out
) -> None:
    color_palette = [
        (248, 24, 148),
        (130, 0, 0),
        (255, 0, 0),
        (0, 112, 0),
        (0, 251, 71),
        (223, 18, 255),
        (0, 0, 0),
        (120, 120, 120),
        (0, 0, 255),
        (3, 185, 213),
        (255, 115, 40),
        (255, 253, 55)
    ]
    for note in notes:
        if note.local_tonic is None or note.pitch is None:
            continue
        (x1, y1, x2, y2) = note.bbox
        center_x = x1 + abs(x1 - x2) / 2
        center_y = y1 + abs(y1 - y2) / 2

        a = calculate_angle_between_vectors((x2 - x1, y1 - y2),
                                            (1, 0))
        angle = a[1]
        angle_r = a[0]

        axis_x = int((abs(x1 - x2) / 2) / math.cos(angle_r))
        axis_y = int((abs(y1 - y2) / 2) * math.cos(angle_r))

        axes = (axis_x, axis_y)
        if note.label is NoteType.WHOLE or note.label is NoteType.HALF or note.label is NoteType.HALF_OR_WHOLE:
            draw_hollow_note_head(out, (int(center_x), int(center_y)),
                                  color_palette[(note.pitch - note.local_tonic) % 12], axes, int(axes[0] / 5),
                                  360 - angle)
        else:
            draw_note_head(out, (int(center_x), int(center_y)), color_palette[(note.pitch - note.local_tonic) % 12], axes,
                           360 - angle)


def draw_hollow_note_head(image, center, color, axes_length, thickness, angle):
    center_coordinates = center
    start_angle = 0
    end_angle = 360
    cv2.ellipse(image, center_coordinates, axes_length, angle, start_angle, end_angle, color, thickness)


# Function to draw a note head
def draw_note_head(image, center, color, axes_length, angle):
    # Draw the note head (ellipse)
    center_coordinates = center
    start_angle = 0
    end_angle = 360
    cv2.ellipse(image, center_coordinates, axes_length, angle, start_angle, end_angle, color, -1)


def teaser() -> Image.Image:
    print("Начинаем формировать Teaser")
    ori_img = layers.get_layer('original_image')
    notes = layers.get_layer('notes')

    global out
    out = np.copy(ori_img).astype(np.uint8)

    draw_dots(notes, out)

    return Image.fromarray(out)
