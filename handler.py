import dataclasses
import datetime
import uuid
from oemer.ete import main
import boto3
import requests
import json
import os
import ydb
from datetime import datetime, timezone

@dataclasses.dataclass
class ImageHighlightRequest:
    RequestId:str
    ProcessId:str
    ImageLink:str

@dataclasses.dataclass
class Args:
    img_path:str
    use_tf:bool
    save_cache:bool
    without_deskew:bool

def ticks(dt):
    t = (datetime(9999, 12, 31, 23, 59, 59, 999999) - dt.replace(tzinfo=None)).total_seconds() * 10000000
    return int(t)

def worker():
    files = os.listdir('.')
    for file in files:
        if file.endswith('.jpg') or file.endswith('.pkl'):
            os.remove(file)
    while True:
        print("Начинаем читать сообщения из очереди")
        client = boto3.client(
            service_name='sqs',
            endpoint_url='https://message-queue.api.cloud.yandex.net',
            region_name='ru-central1'
        )
        queue_url = "https://message-queue.api.cloud.yandex.net/b1gbr73igbk1l6jeq1li/dj600000001k6gjv02nu/image-highlight-request"
        # Receive sent message
        base_files = []
        handled_files = []
        links = []
        messages = client.receive_message(
            QueueUrl=queue_url,
            MaxNumberOfMessages=10,
            VisibilityTimeout=60,
            WaitTimeSeconds=20
        ).get('Messages')
        if not messages:
            print("Нет сообщений")
            continue
        print(f"Получено сообщений: {len(messages)}")
        for bmsg in messages:
            try:
                msg = json.loads(bmsg['Body'])
                base, handled = handle_message(ImageHighlightRequest(msg['RequestId'], msg['ProcessId'], msg['ImageLink']), client, bmsg, queue_url, msg['ProcessId'])
                base_files.append(base)
                handled_files.append(handled)
                print("Загружаем в S3")
                link = load_file_to_s3(handled)
                links.append((msg['ProcessId'], link))
                try:
                    os.remove(handled)
                except:
                    pass
                os.remove(base)
                print("Пишем в бд")
                endpoint = "grpcs://ydb.serverless.yandexcloud.net:2135"
                database = "/ru-central1/b1gbr73igbk1l6jeq1li/etn9ru8i2hg6c4qik44h"
                driver_config = ydb.DriverConfig(
                    endpoint, database, credentials=ydb.credentials_from_env_variables()
                )
                with ydb.Driver(driver_config) as driver:
                    try:
                        driver.wait(timeout=5)
                        session = driver.table_client.session().create()
                        query = f"""
                                    DECLARE $link as String;
                                    DECLARE $timestamp as Timestamp;
                                    DECLARE $id as String;
                                    UPDATE image_status_db
                                    SET timestamp = $timestamp, status = 3, ready_image_link = $link, base_image_link=base_image_link
                                    where id=$id;
                                    """
                        p_query = session.prepare(query)
                        session.transaction(ydb.SerializableReadWrite()).execute(
                            p_query, {
                                '$link': str.encode(link),
                                '$timestamp': int((datetime.now(timezone.utc).replace(
                                    tzinfo=None) - datetime.utcfromtimestamp(0)).total_seconds() * 1000000),
                                '$id': str.encode(msg['ProcessId']),
                            },
                            commit_tx=True
                        )
                    except TimeoutError:
                        print("Connect failed to YDB")
                        print("Last reported errors by discovery:")
                        print(driver.discovery_debug_details())

                    client.delete_message(
                        QueueUrl=queue_url,
                        ReceiptHandle=bmsg.get('ReceiptHandle')
                    )
            except TimeoutError as e:
                pass

def load_file_to_s3(file_path):
    session = boto3.session.Session()
    s3 = session.client(
        service_name='s3',
        endpoint_url='https://storage.yandexcloud.net'
    )
    ## Из строки
    link = s3.upload_file(file_path, 'music-syntax-highlight-images', file_path)
    link = f"https://storage.yandexcloud.net/music-syntax-highlight-images/{file_path}"
    return link

def handle_message(message:ImageHighlightRequest, client, msg, queue_url, process_id):
    response = requests.get(message.ImageLink)
    file_name = f"target_{uuid.uuid4()}.jpg"
    with open(file_name, 'wb') as file:
        file.write(response.content)
    try:
        main(Args(file_name, True, True,False))
    except BaseException as e:
        print(e)
        handle_error(client, msg, queue_url, process_id)
    return file_name, file_name.replace(".jpg", "_teaser.png")

def handle_error(client, msg, queue_url, process_id):
    print("Пишем ошибку в бд")
    endpoint = "grpcs://ydb.serverless.yandexcloud.net:2135"
    database = "/ru-central1/b1gbr73igbk1l6jeq1li/etn9ru8i2hg6c4qik44h"
    driver_config = ydb.DriverConfig(
        endpoint, database, credentials=ydb.credentials_from_env_variables()
    )
    with ydb.Driver(driver_config) as driver:
        try:
            driver.wait(timeout=5)
            session = driver.table_client.session().create()
            query = f"""
                                                          DECLARE $timestamp as Timestamp;
                                                          DECLARE $id as String;
                                                          UPDATE image_status_db
                                                          SET timestamp = $timestamp, status = 0, ready_image_link = ready_image_link, base_image_link=base_image_link
                                                          where id=$id;
                                                          """
            p_query = session.prepare(query)
            session.transaction(ydb.SerializableReadWrite()).execute(
                p_query, {
                    '$timestamp': int((datetime.now(timezone.utc).replace(
                        tzinfo=None) - datetime.utcfromtimestamp(0)).total_seconds() * 1000000),
                    '$id': str.encode(process_id),
                },
                commit_tx=True
            )
        except TimeoutError:
            print("Connect failed to YDB")
            print("Last reported errors by discovery:")
            print(driver.discovery_debug_details())
    client.delete_message(
        QueueUrl=queue_url,
        ReceiptHandle=msg.get('ReceiptHandle')
    )

worker()
